import type { NextApiRequest, NextApiResponse } from "next"
import { getSheet } from "utils/google/auth"

import { Leads } from "types/leads"

const mountData = (data: Leads, opp: boolean) => {
  const date = new Date()
  const lead = data.leads[0]

  const formatedDate =
    date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()

  const _data = {
    Timestamp: formatedDate,
    Email: lead?.email,
    Telefone: lead?.personal_phone || lead?.mobile_phone,
    Nome: lead?.name,
    Cargo: lead?.job_title,
    EstagioLead: lead?.lead_stage || null,
    Identificador: lead?.last_conversion.content?.identificador || null,
    Origem: lead?.last_conversion.conversion_origin?.channel || "Direct",
    Source: lead?.last_conversion.conversion_origin?.source || null,
    Medium: lead?.last_conversion.conversion_origin?.medium || null,
    Campanha: lead?.last_conversion.conversion_origin?.campaign || null,
    Oportunidade: opp ? 1 : 0,
    Conversão: opp ? 0 : 1,
    LeadNovo: lead.number_conversions === 0 ? 1 : 0,
    // Tag: lead.tags[0] || null,
    // Tag2: lead.tags[1] || null,
    // Tag3: lead.tags[2] || null,
    // Tag4: lead.tags[3] || null,
    // Tag5: lead.tags[4] || null
  }
  //https://integracao-rdstation.vercel.app/api/rdstation/1iSgCgCBC7mdTud3xX2m2aMpGPrkNbllqntgnYpuz2WA
  return _data
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { pid, opp } = req.query
  const _opp = opp === "true" ? true : false

  const data = req.body
  const sheet = await getSheet(pid as string)

  const row = mountData(data, _opp)
  await sheet.addRow(row, { insert: true })

  console.log("response", row)
  res.end(`Post: ${pid}`)
}
