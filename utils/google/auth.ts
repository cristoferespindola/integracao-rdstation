const { GoogleSpreadsheet } = require("google-spreadsheet")
const credentials = require("./cred.json")

export const getSheet = async (id: string) => {
  const doc = new GoogleSpreadsheet(id)

  try {
    await doc.useServiceAccountAuth(credentials)
    await doc.loadInfo()

    return doc.sheetsByIndex[0]
  } catch (err) {
    console.error(err)

    return null
  }
}


