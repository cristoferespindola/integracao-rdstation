/*
{"leads":[{"id":"1186980208","email":"teste2@teste.com","name":"Teste Dev","company":"teste","job_title":"teste","bio":null,"public_url":"http://app.rdstation.com.br/leads/public/9e024c6f-200f-4a69-bb50-ea2b83c3fc75","created_at":"2020-10-28T10:45:05.501-03:00","opportunity":"true","number_conversions":"4","user":"comercial@aquila.marketing","first_conversion":{"content":{"_wpcf7_container_post":"421","nome":"Teste Dev","cargo":"teste","empresa":"teste","site":"http://teste.com","tel":"99999999","mensagem":"teste","form_origem":"Plugin Contact Form 7","identificador":"orcamento-site","traffic_source":"encoded_eyJmaXJzdF9zZXNzaW9uIjp7InZhbHVlIjoiaHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8iLCJleHRyYV9wYXJhbXMiOnt9fSwiY3VycmVudF9zZXNzaW9uIjp7InZhbHVlIjoiKG5vbmUpIiwiZXh0cmFfcGFyYW1zIjp7fX0sImNyZWF0ZWRfYXQiOjE2MDM4OTI2ODE3MzR9","created_at":"2020-10-28T13:45:05.501148Z","email_lead":"teste@teste.com","UF":null},"created_at":"2020-10-28T10:45:05.501-03:00","cumulative_sum":"1","source":"orcamento-site","conversion_origin":{"source":"(direct)","medium":"(none)","value":null,"campaign":"(direct)","channel":"Direct"}},"last_conversion":{"content":{"_wpcf7_container_post":"421","nome":"Teste Dev","cargo":"teste","empresa":"teste","site":"http://teste.com","tel":"99999999","mensagem":"eee","form_origem":"Plugin Contact Form 7","identificador":"orcamento-site","tags":["integrar"],"traffic_source":"encoded_eyJmaXJzdF9zZXNzaW9uIjp7InZhbHVlIjoiaHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8iLCJleHRyYV9wYXJhbXMiOnt9fSwiY3VycmVudF9zZXNzaW9uIjp7InZhbHVlIjoiKG5vbmUpIiwiZXh0cmFfcGFyYW1zIjp7fX0sImNyZWF0ZWRfYXQiOjE2MDM4OTI5MDExNzF9","created_at":"2020-10-28T13:59:05.745847Z","email_lead":"teste@teste.com","UF":null},"created_at":"2020-10-28T10:59:05.745-03:00","cumulative_sum":"4","source":"orcamento-site","conversion_origin":{"source":"(direct)","medium":"(none)","value":null,"campaign":"(direct)","channel":"Direct"}},"custom_fields":{},"website":null,"personal_phone":null,"mobile_phone":null,"city":null,"state":null,"tags":["integrar"],"lead_stage":"Lead","last_marked_opportunity_date":"2020-10-28T10:46:08.424-03:00","uuid":"9e024c6f-200f-4a69-bb50-ea2b83c3fc75","fit_score":"d","interest":0}]}
*/
/*
      "conversion": {
        "content": {
          "_wpcf7_container_post": "421",
          "nome": "Teste Dev",
          "cargo": "teste",
          "empresa": "teste",
          "site": "http://teste.com",
          "tel": "99999999",
          "mensagem": "eee",
          "form_origem": "Plugin Contact Form 7",
          "identificador": "orcamento-site",
          "tags": ["integrar"],
          "traffic_source": "encoded_eyJmaXJzdF9zZXNzaW9uIjp7InZhbHVlIjoiaHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8iLCJleHRyYV9wYXJhbXMiOnt9fSwiY3VycmVudF9zZXNzaW9uIjp7InZhbHVlIjoiKG5vbmUpIiwiZXh0cmFfcGFyYW1zIjp7fX0sImNyZWF0ZWRfYXQiOjE2MDM4OTI5MDExNzF9",
          "created_at": "2020-10-28T13:59:05.745847Z",
          "email_lead": "teste@teste.com",
          "UF": null
        },
        "created_at": "2020-10-28T10:59:05.745-03:00",
        "cumulative_sum": "4",
        "source": "orcamento-site",
        "conversion_origin": {
          "source": "(direct)",
          "medium": "(none)",
          "value": null,
          "campaign": "(direct)",
          "channel": "Direct"
        }
      }
*/
export interface Conversion {
    content: {
        _wpcf7_container_post: string
        nome: string
        cargo: string
        empresa: string
        site: string
        tel: string
        mensagem: string
        form_origem: string
        identificador: string
        tags: string[],
        traffic_source: string
        created_at: string
        email_lead: string
        UF: string
    },
    created_at: string
    cumulative_sum: string
    source: string
    conversion_origin: {
        source: string
        medium: string
        value: string
        campaign: string
        channel: string
    }
}


export interface Lead {
    id: string
    email: string
    name: string
    company: string
    job_title: string
    lead_stage: string
    opportunity: true,
    number_conversions: number
    bio: string
    public_url: string
    created_at: string
    last_marked_opportunity_date: string
    personal_phone: string
    mobile_phone: string
    tags: string[]
    traffic_source: string
    first_conversion: Conversion
    last_conversion: Conversion
}

export interface Leads {
    leads: Lead[]
}

